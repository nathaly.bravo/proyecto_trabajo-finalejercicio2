/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.DAO;

import controlador.tda.lista.ListaEnlazadaServices;
import modelo.BienInmueble;

/**
 *
 * @author Nathaly
 */
public class InmuebleServicio {
    private InmuebleDao inm = new InmuebleDao();

    /**
     *
     * @return
     */
    public BienInmueble getBien() {
        return inm.getBien();
    }
    
    public ListaEnlazadaServices<BienInmueble> getLista() {
        return inm.getListaBienes();
    }
    
    public ListaEnlazadaServices<BienInmueble> getListaArchivo() {
        return inm.listado();
    }
    
    public Boolean guardar() {
       return inm.guardar();
    }
    
    public Boolean modificar(Integer pos) {
        return inm.modificar(pos);
    }
    
      public Boolean eliminar (Integer pos) {
        return inm.eliminar(pos);
    }
    
    public void setBien(BienInmueble bien) {
        inm.setBien(bien);
    }
}
