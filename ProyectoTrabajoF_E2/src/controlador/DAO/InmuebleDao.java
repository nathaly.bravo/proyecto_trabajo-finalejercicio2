/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.DAO;

import controlador.tda.lista.ListaEnlazadaServices;
import modelo.BienInmueble;

/**
 *
 * @author Nathaly
 */
public class InmuebleDao extends AdaptadorDao<BienInmueble> {
    private BienInmueble bienInmueble;
    private ListaEnlazadaServices<BienInmueble> listabienes;

    public InmuebleDao() {
        super(BienInmueble.class);
        listado();
    }

    public ListaEnlazadaServices<BienInmueble> getListaBienes() {

        return listabienes;
    }

    public void setListaBienes(ListaEnlazadaServices<BienInmueble> listabienes) {
        this.listabienes = listabienes;
    }

    public BienInmueble getBien() {
        if (this.bienInmueble == null) {
            this.bienInmueble = new BienInmueble();
        }
        return bienInmueble;
    }

    public void setBien(BienInmueble bien) {
        this.bienInmueble= bien;
    }
   public Boolean guardar() {
        try {            
            getBien().setId(listabienes.getSize()+1);
            guardar(getBien());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar bien inmueble"+e);
        }
        return false;
    }
    
    public Boolean modificar(Integer pos) {
        try {            
            
            modificar(getBien(), pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en modificar bien inmueble"+e);
        }
        return false;
    }

    /**
     *
     * @param pos
     * @return
     */
    public Boolean eliminar(Integer pos) {
        try {            
            listabienes.eliminarPosicion(pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en modificar bien inmueble"+e);
        }
        return false;
    }

    public ListaEnlazadaServices<BienInmueble> listado() {
        setListaBienes(listar());
        return listabienes;
    }
}
