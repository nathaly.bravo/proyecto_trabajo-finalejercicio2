/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.DAO;

import controlador.tda.lista.ListaEnlazadaServices;
import modelo.Cuenta;

/**
 *
 * @author DELL
 */
public class CuentaDao extends AdaptadorDao<Cuenta> {

    private Cuenta cuenta;
    private ListaEnlazadaServices<Cuenta> usuario;

    public CuentaDao() {
        super(Cuenta.class);
    }

    public Cuenta getCuenta() {
        if (cuenta == null) {
            cuenta = new Cuenta();
        }
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public ListaEnlazadaServices<Cuenta> getUsuario() {
        return usuario;
    }

    public void setUsuario(ListaEnlazadaServices<Cuenta> usuario) {
        this.usuario = usuario;
    }

    public boolean guardar() {
        try {
            getCuenta().setId(usuario.getSize() + 1);
            guardar(getCuenta());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar usuario");
        }
        return false;
    }

    public ListaEnlazadaServices<Cuenta> listado() {
        setUsuario(listar());
        return usuario;
    }

    public Cuenta buscarCuentaUser(String dato) {
        Cuenta c = null;
        ListaEnlazadaServices<Cuenta> lista = listar();
        for (int i = 0; i < listar().getSize(); i++) {
            if (dato.toLowerCase().equals(lista.obtenerDato(i).getUser())) {
                c = lista.obtenerDato(i);
            }
        }
        return c;
    }

    public Cuenta inicioSecion(String user, String password) {
        Cuenta c = buscarCuentaUser(user);
        return (c != null && c.getPassword().equals(password)) ? c : null;
    }
    public ListaEnlazadaServices<Cuenta> ordenar() {
        try {
            ListaEnlazadaServices<Cuenta> lista = listar();
            int intercambio = 0;
            for (int i = 0; i < lista.getSize()- 1; i++) {
                int k = i;
                Cuenta t = lista.obtenerDato(i);//datos[i];            
                for (int j = i + 1; j < lista.getSize(); j++) {
                    if (lista.obtenerDato(j).getUser().toLowerCase().compareTo(t.getUser().toLowerCase()) < 0) {
                        t = lista.obtenerDato(j);
                        k = j;
                        intercambio++;
                    }
                }
                lista.modificarDatoPosicion(k, lista.obtenerDato(i));
                lista.modificarDatoPosicion(i, t);
            }
            return lista;
        } catch (Exception e) {
            return new ListaEnlazadaServices<>();
        }
    }

}
