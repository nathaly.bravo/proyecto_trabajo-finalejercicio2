/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.DAO.InmuebleServicio;
import controlador.utiles.Utilidades;
import javax.swing.JOptionPane;
import vista.modeloTablas.ModeloTablaBienes;

/**
 *
 * @author Nathaly
 */
public class Frm_BienesInm extends javax.swing.JFrame {

    private ModeloTablaBienes mc = new ModeloTablaBienes();
    private InmuebleServicio inmueble = new InmuebleServicio();
    boolean estado = false;
    private int accionTabla = 0;
    private Integer idBuscar;
    private int pos = -1;
    private int fila = -1;

    /**
     * Creates new form Frm_Bienes
     */
    public Frm_BienesInm() {
        initComponents();
        CargarTabla();
        this.cbxtipo.removeAllItems();
        for (String aux : Utilidades.tipos()) {
            this.cbxtipo.addItem(aux);
        }
        cbxtipo.updateUI();
        Inicio();
        
    }

    //Metodo Cargar Tabla
    public void CargarTabla() {
        mc.setLista(inmueble.getListaArchivo());
        tbl_bienes.setModel(mc);
        tbl_bienes.updateUI();
    }

    //Metodo Nuevo
    public void Limpiar() {
        txtdescrp.setText("");
        txtregistro.setText("");
        txtubi.setText("");
        txtvalor.setText("");
    }

    //Metodo Eliminar
    private void eliminar() {
        fila = tbl_bienes.getSelectedRow();
        try {
            if (fila >= 0) {
                inmueble.eliminar(fila);
                JOptionPane.showMessageDialog(null, "Se elimino correctamente", "Listo", JOptionPane.INFORMATION_MESSAGE);
                CargarTabla();
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un registro de la tabla", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error del sistema", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    //Metoddo editar
    private void modificar() {
        BuscarCargarDatos();
        tbl_bienes.setEnabled(false);
        BtnGuardar.setText("Actualizar");
    }

    private void Inicio() {
        setTitle("BIEN INMUEBLE");
        estado = false;
        CargarTabla();
        Limpiar();
    }

    private void BuscarCargarDatos() {
        int fila = tbl_bienes.getSelectedRow();
        if (fila >= 0) {
            inmueble.setBien(mc.getLista().obtenerDato(fila));
            if (inmueble.getBien().getId() != null) {
                idBuscar = inmueble.getBien().getId();
                txtregistro.setText(inmueble.getBien().getNro_Registro().toString());
                txtubi.setText(inmueble.getBien().getUbicacion());
                txtvalor.setText(inmueble.getBien().getValor().toString());
                cbxclasif.setSelectedItem(inmueble.getBien().getClasificacion());
                cbxservici.setSelectedItem(inmueble.getBien().getServicios());
                txtdescrp.setText(inmueble.getBien().getDescripcion());

                int tipo = Utilidades.tipos().length;
                for (String aux : Utilidades.tipos()) {
                    for (int i = 0; i < tipo; i++) {
                        if (aux.equals(inmueble.getBien().getTipo())) {

                            this.cbxtipo.setSelectedItem(i);
                        }
                    }

                    cbxtipo.updateUI();

                }
            }
        }

    }

    public void guardar() {
        if (txtregistro.getText().trim().isEmpty() || txtubi.getText().trim().isEmpty() || txtvalor.getText().trim().isEmpty() || txtdescrp.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            inmueble.getBien().setNro_Registro(txtregistro.getText().trim());
            inmueble.getBien().setUbicacion(txtubi.getText().trim());
            inmueble.getBien().setValor(txtvalor.getText().trim());
            inmueble.getBien().setClasificacion(cbxclasif.getSelectedItem().toString());
            inmueble.getBien().setServicios(cbxservici.getSelectedItem().toString());
            inmueble.getBien().setTipo(cbxtipo.getSelectedItem().toString());
            inmueble.getBien().setDescripcion(txtdescrp.getText().trim());

            if (inmueble.getBien().getId() == null) {
                if (inmueble.guardar()) {
                    JOptionPane.showMessageDialog(null, "Se ha guardadao correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
                    CargarTabla();
                    Limpiar();
                    
                } else {
                    JOptionPane.showMessageDialog(null, "No se guardo", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                if (inmueble.modificar(pos)) {

                    JOptionPane.showMessageDialog(null, "Se ha modificado correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
                    Limpiar();
                    CargarTabla();
                } else {
                    JOptionPane.showMessageDialog(null, "No se pudo modificar", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        tabdatos = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtregistro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtubi = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtvalor = new javax.swing.JTextField();
        cbxservici = new javax.swing.JComboBox<>();
        BtnGuardar = new javax.swing.JButton();
        BtnCancel = new javax.swing.JButton();
        BtnNuevo = new javax.swing.JButton();
        cbxclasif = new javax.swing.JComboBox<>();
        txtdescrp = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        cbxtipo = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_bienes = new javax.swing.JTable();
        BtnEliminar = new javax.swing.JButton();
        BtnModificar = new javax.swing.JButton();

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/users-icon.png"))); // NOI18N

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 204, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/agency-icon.png"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 0, 40, 50));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Bien Inmueble");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 10, 220, 30));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 790, 20));

        tabdatos.setBackground(new java.awt.Color(255, 255, 153));
        tabdatos.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N
        tabdatos.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel1.setText("Clasificación:");
        tabdatos.add(jLabel1);
        jLabel1.setBounds(303, 30, 80, 30);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel3.setText("Nro Registro:");
        tabdatos.add(jLabel3);
        jLabel3.setBounds(16, 30, 80, 30);

        txtregistro.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        tabdatos.add(txtregistro);
        txtregistro.setBounds(100, 30, 176, 30);

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel6.setText("Ubicación:");
        tabdatos.add(jLabel6);
        jLabel6.setBounds(16, 86, 80, 30);

        txtubi.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        tabdatos.add(txtubi);
        txtubi.setBounds(100, 86, 176, 30);

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel7.setText("Servicios:");
        tabdatos.add(jLabel7);
        jLabel7.setBounds(303, 70, 89, 30);

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel8.setText("Tipo:");
        tabdatos.add(jLabel8);
        jLabel8.setBounds(303, 110, 130, 30);

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel10.setText("Valor:");
        tabdatos.add(jLabel10);
        jLabel10.setBounds(16, 140, 80, 30);

        txtvalor.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        tabdatos.add(txtvalor);
        txtvalor.setBounds(101, 140, 175, 30);

        cbxservici.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        cbxservici.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "LUZ - AGUA", "LUZ - AGUA - ALCANTARILLADO", "TODOS" }));
        cbxservici.setBorder(null);
        cbxservici.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxserviciActionPerformed(evt);
            }
        });
        tabdatos.add(cbxservici);
        cbxservici.setBounds(402, 70, 182, 30);

        BtnGuardar.setBackground(new java.awt.Color(153, 204, 255));
        BtnGuardar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        BtnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/Actions-document-save-all-icon (1).png"))); // NOI18N
        BtnGuardar.setText("GUARDAR");
        BtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGuardarActionPerformed(evt);
            }
        });
        tabdatos.add(BtnGuardar);
        BtnGuardar.setBounds(618, 32, 133, 41);

        BtnCancel.setBackground(new java.awt.Color(153, 204, 255));
        BtnCancel.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        BtnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/Cancel-icon.png"))); // NOI18N
        BtnCancel.setText("CANCELAR");
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });
        tabdatos.add(BtnCancel);
        BtnCancel.setBounds(618, 93, 133, 41);

        BtnNuevo.setBackground(new java.awt.Color(153, 204, 255));
        BtnNuevo.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        BtnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/Actions-appointment-new-icon.png"))); // NOI18N
        BtnNuevo.setText("NUEVO");
        BtnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNuevoActionPerformed(evt);
            }
        });
        tabdatos.add(BtnNuevo);
        BtnNuevo.setBounds(618, 152, 133, 41);

        cbxclasif.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        cbxclasif.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "RURAL", "URBANO" }));
        cbxclasif.setBorder(null);
        tabdatos.add(cbxclasif);
        cbxclasif.setBounds(401, 30, 182, 30);

        txtdescrp.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        tabdatos.add(txtdescrp);
        txtdescrp.setBounds(400, 150, 190, 30);

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel9.setText("Descripción:");
        tabdatos.add(jLabel9);
        jLabel9.setBounds(300, 150, 91, 27);

        cbxtipo.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        cbxtipo.setBorder(null);
        cbxtipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxtipoActionPerformed(evt);
            }
        });
        tabdatos.add(cbxtipo);
        cbxtipo.setBounds(404, 110, 182, 30);

        jPanel1.add(tabdatos, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 770, 210));

        jPanel2.setBackground(new java.awt.Color(255, 255, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Lista Bienes Inmuebles", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N

        tbl_bienes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tbl_bienes);

        BtnEliminar.setBackground(new java.awt.Color(102, 204, 255));
        BtnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/Status-dialog-error-icon (1).png"))); // NOI18N
        BtnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEliminarActionPerformed(evt);
            }
        });

        BtnModificar.setBackground(new java.awt.Color(102, 204, 255));
        BtnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/Actions-document-properties-icon.png"))); // NOI18N
        BtnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnModificarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 738, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(BtnModificar)
                        .addGap(18, 18, 18)
                        .addComponent(BtnEliminar)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnEliminar)
                    .addComponent(BtnModificar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, 770, 240));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGuardarActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_BtnGuardarActionPerformed

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        // TODO add your handling code here:
        Inicio();
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void BtnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNuevoActionPerformed
        // TODO add your handling code here:
        Limpiar();
    }//GEN-LAST:event_BtnNuevoActionPerformed

    private void cbxserviciActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxserviciActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxserviciActionPerformed

    private void cbxtipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxtipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxtipoActionPerformed

    private void BtnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEliminarActionPerformed
        // TODO add your handling code here:
        eliminar();
    }//GEN-LAST:event_BtnEliminarActionPerformed

    private void BtnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnModificarActionPerformed
        // TODO add your handling code here:
        modificar();
    }//GEN-LAST:event_BtnModificarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_BienesInm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_BienesInm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_BienesInm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_BienesInm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_BienesInm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnEliminar;
    private javax.swing.JButton BtnGuardar;
    private javax.swing.JButton BtnModificar;
    private javax.swing.JButton BtnNuevo;
    private javax.swing.JComboBox<String> cbxclasif;
    private javax.swing.JComboBox<String> cbxservici;
    private javax.swing.JComboBox<String> cbxtipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTable jTable1;
    private javax.swing.JPanel tabdatos;
    private javax.swing.JTable tbl_bienes;
    private javax.swing.JTextField txtdescrp;
    private javax.swing.JTextField txtregistro;
    private javax.swing.JTextField txtubi;
    private javax.swing.JTextField txtvalor;
    // End of variables declaration//GEN-END:variables
}
